<?php
require_once 'Excel/reader.php';
require_once 'config.php';
$data = new Spreadsheet_Excel_Reader();
$data->setOutputEncoding('CP1251');
$data->read('CIE10-ES1.xls');
echo "<h1>Excel 1</h1>";
echo("<table>");
for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) { //8000
	echo("<tr>");
	for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
		echo "<td>", $data->sheets[0]['cells'][$i][$j];
		if($j == 1){
			$diagnostic = "";
			$code = $data->sheets[0]['cells'][$i][$j];
			//selecting data associated with this particular id
			$result = $db->icd10_diagnosis->findOne(array('code' => $code));

			if(count($result) > 0) {
				$diagnostic = $result['diagnostic'];
				
			}
		} else {
			if($diagnostic != "") {
				echo " -- " . $diagnostic;
			}
		}
		echo "</td>";
	}
	echo("</tr>");
}
echo("</table>");